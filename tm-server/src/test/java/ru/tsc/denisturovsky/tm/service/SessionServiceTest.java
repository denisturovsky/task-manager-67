package ru.tsc.denisturovsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.dto.ISessionDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.exception.field.IdEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.UserIdEmptyException;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;

import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.SessionTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private final static ISessionDTOService SERVICE = CONTEXT.getBean(ISessionDTOService.class);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.add(null, USER_SESSION3));
        Assert.assertNotNull(SERVICE.add(USER_ID, USER_SESSION3));
        @Nullable final SessionDTO session = SERVICE.findOneById(USER_ID, USER_SESSION3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION3.getId(), session.getId());
    }

    @After
    public void after() throws Exception {
        SERVICE.clear(USER_ID);
    }

    @Before
    public void before() throws Exception {
        SERVICE.add(USER_ID, USER_SESSION1);
        SERVICE.add(USER_ID, USER_SESSION2);
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.clear(""));
        SERVICE.clear(USER_ID);
        Assert.assertEquals(0, SERVICE.getSize(USER_ID));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", NON_EXISTING_SESSION_ID));
        Assert.assertFalse(SERVICE.existsById(USER_ID, ""));
        Assert.assertFalse(SERVICE.existsById(USER_ID, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(SERVICE.existsById(USER_ID, USER_SESSION1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.findAll(""));
        final List<SessionDTO> sessions = SERVICE.findAll(USER_ID);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(USER_ID, session.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.findOneById(USER_ID, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", USER_SESSION1.getId()));
        Assert.assertNull(SERVICE.findOneById(USER_ID, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = SERVICE.findOneById(USER_ID, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.getSize(""));
        Assert.assertEquals(2, SERVICE.getSize(USER_ID));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeOneById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeOneById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeOneById(USER_ID, null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeOneById(USER_ID, ""));
        SERVICE.removeOneById(USER_ID, USER_SESSION2.getId());
        Assert.assertNull(SERVICE.findOneById(USER_ID, USER_SESSION2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        SERVICE.remove(USER_ID, USER_SESSION2);
        Assert.assertNull(SERVICE.findOneById(USER_ID, USER_SESSION2.getId()));
    }

}