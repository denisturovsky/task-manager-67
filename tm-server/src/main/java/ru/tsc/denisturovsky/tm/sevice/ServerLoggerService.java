package ru.tsc.denisturovsky.tm.sevice;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.denisturovsky.tm.api.service.IServerLoggerService;
import ru.tsc.denisturovsky.tm.listener.JmsLoggerProducer;
import ru.tsc.denisturovsky.tm.listener.ServerEntityListener;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.logging.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ServerLoggerService implements IServerLoggerService {

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMANDS_FILE = "./commands.xml";

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = "./errors.xml";

    @NotNull
    private static final Logger LOGGER_COMMAND = getLoggerCommands();

    @NotNull
    private static final Logger LOGGER_ERROR = getLoggerErrors();

    @NotNull
    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    @NotNull
    private static final LogManager MANAGER = LogManager.getLogManager();

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final Logger LOGGER_MESSAGE = getLoggerMessages();

    @NotNull
    private static final String MESSAGES_FILE = "./messages.xml";

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    @NotNull
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    {
        init();
        registry(LOGGER_COMMAND, COMMANDS_FILE, false);
        registry(LOGGER_ERROR, ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGES_FILE, true);
    }

    @NotNull
    public static Logger getLoggerCommands() {
        return Logger.getLogger(COMMANDS);
    }

    @NotNull
    public static Logger getLoggerErrors() {
        return Logger.getLogger(ERRORS);
    }

    @NotNull
    public static Logger getLoggerMessages() {
        return Logger.getLogger(MESSAGES);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {

            public String format(LogRecord record) {

                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    private void init() {
        try {
            MANAGER.readConfiguration(ServerLoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void initJmsLogger() {
        @NotNull final JmsLoggerProducer jmsLoggerProducer = new JmsLoggerProducer();
        @NotNull final ServerEntityListener serverEntityListener = new ServerEntityListener(jmsLoggerProducer);
        @NotNull final SessionFactoryImpl sessionFactoryImpl = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry eventListenerRegistry = sessionFactoryImpl.getServiceRegistry().getService(
                EventListenerRegistry.class);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(serverEntityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(serverEntityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(serverEntityListener);
    }

    private void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

}