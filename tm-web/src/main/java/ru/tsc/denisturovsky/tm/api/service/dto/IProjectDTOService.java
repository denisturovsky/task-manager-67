package ru.tsc.denisturovsky.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.enumerated.Status;

import java.util.List;

public interface IProjectDTOService {

    @NotNull
    ProjectDTO add(@NotNull ProjectDTO model) throws Exception;

    void changeProjectStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void clear() throws Exception;

    int count() throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String name) throws Exception;

    @NotNull
    ProjectDTO create(
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<ProjectDTO> findAll() throws Exception;

    @Nullable
    ProjectDTO findOneById(@Nullable String id) throws Exception;

    void remove(@Nullable ProjectDTO model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @NotNull
    ProjectDTO update(@Nullable ProjectDTO model) throws Exception;

    void updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}