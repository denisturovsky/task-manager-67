package ru.tsc.denisturovsky.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.util.DateUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @Column
    @NotNull
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date created = new Date();

    @Nullable
    @Column(name = "date_begin")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateEnd;

    public TaskDTO(@NotNull final String name) {
        this.name = name;
    }

    public TaskDTO(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    public TaskDTO(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) {
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @NotNull
    @Override
    public String toString() {
        return String.format(
                "%30s:%30s:%30s:%30s:%30s|",
                name,
                getStatus().getDisplayName(),
                description,
                DateUtil.toString(getDateBegin()),
                DateUtil.toString(getDateEnd())
        );
    }

}