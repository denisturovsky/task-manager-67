package ru.tsc.denisturovsky.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.enumerated.Status;

import java.util.List;

public interface ITaskDTOService {

    @NotNull
    TaskDTO add(@NotNull TaskDTO model) throws Exception;

    void changeTaskStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void clear() throws Exception;

    int count() throws Exception;

    @NotNull
    TaskDTO create(@Nullable String name) throws Exception;

    @NotNull
    TaskDTO create(
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<TaskDTO> findAll() throws Exception;

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable String projectId
    ) throws Exception;

    @Nullable
    TaskDTO findOneById(@Nullable String id) throws Exception;

    void remove(@Nullable TaskDTO model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @Nullable
    TaskDTO update(@Nullable TaskDTO model) throws Exception;

    void updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateProjectIdById(
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception;

}
